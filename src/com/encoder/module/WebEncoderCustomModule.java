package com.encoder.module;

import com.wowza.wms.amf.AMFDataList;
import com.wowza.wms.application.IApplicationInstance;
import com.wowza.wms.authentication.IAuthenticateUsernamePasswordProvider;
import com.wowza.wms.authentication.file.AuthenticationPasswordFiles;
import com.wowza.wms.client.IClient;
import com.wowza.wms.request.RequestFunction;
import com.wowza.wms.security.ModuleCoreSecurity;
import com.wowza.wms.stream.IMediaStream;
import org.apache.commons.lang.StringUtils;

import java.io.File;

public class WebEncoderCustomModule extends ModuleCoreSecurity {

    private final static String FLASH_CONNECTION = "flash_app";
    public static final String AUTHPASSWORDFILEPATH = "${com.wowza.wms.context.VHostConfigHome}/conf/publish.password";
    private File passwordFile = null;

    private ActionNotify actionNotify;

    @Override
    public void onAppStart(IApplicationInstance appInstance) {
        super.onAppStart(appInstance);
        this.actionNotify = new ActionNotify();

        String passwordFileStr = appInstance.decodeStorageDir(AUTHPASSWORDFILEPATH);
        passwordFileStr = appInstance.decodeStorageDir(passwordFileStr);
        passwordFile = new File(passwordFileStr);
    }

    @Override
    public void onAppStop(IApplicationInstance iApplicationInstance) {
        super.onAppStop(iApplicationInstance);
        RecordingHelper.getInstance().release();
    }

    @Override
    public void onConnect(IClient client, RequestFunction function, AMFDataList params) {
        if (!FLASH_CONNECTION.equals(params.getString(PARAM3))) {
            super.onConnect(client, function, params);
            return;
        }
        boolean isAuthenticated = false;
        String username = null;
        String password = null;
        try {
            while (true) {
                if (params.size() <= PARAM3) {
                    break;
                }
                username = params.getString(PARAM1);
                password = params.getString(PARAM2);
                if (username == null || password == null) {
                    break;
                }
                IAuthenticateUsernamePasswordProvider filePasswordProvider = null;
                filePasswordProvider = AuthenticationPasswordFiles.getInstance().getPasswordFile(passwordFile);
                if (filePasswordProvider == null) {
                    break;
                }
                filePasswordProvider.setClient(client);
                String userPassword = filePasswordProvider.getPassword(username);
                if (userPassword == null) {
                    break;
                }
                if (!userPassword.equals(password)) {
                    break;
                }
                isAuthenticated = true;
                break;
            }
        }
        catch (Exception e) {
            getLogger().error("WebEncoderCustomModule.onConnect: " + e.toString());
            isAuthenticated = false;
        }
        getLogger().info("WebEncoderCustomModule Authenticated: " + isAuthenticated);
        if (!isAuthenticated) {
            client.rejectConnection("Authentication Failed[" + client.getClientId() + "]: " + username);
        } else {
            client.getProperties().put("flash", StringUtils.EMPTY);
            client.acceptConnection();
        }
    }

    @Override
    public void onStreamCreate(IMediaStream mediaStream) {
        super.onStreamCreate(mediaStream);
        mediaStream.addClientListener(actionNotify);
    }

    @Override
    public void onStreamDestroy(IMediaStream mediaStream) {
        super.onStreamDestroy(mediaStream);
        mediaStream.removeClientListener(actionNotify);
    }

    @Override
    public void publish(IClient client, RequestFunction function, AMFDataList params) {
        if (!client.getProperties().containsKey("flash")) {
            super.publish(client, function, params);
            return;
        }
        invokePrevious(client, function, params);
    }
}
