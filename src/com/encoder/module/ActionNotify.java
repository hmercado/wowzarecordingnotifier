package com.encoder.module;

import com.wowza.wms.livestreamrecord.model.LiveStreamRecorderMP4;
import com.wowza.wms.logging.WMSLogger;
import com.wowza.wms.logging.WMSLoggerFactory;
import com.wowza.wms.server.Server;
import com.wowza.wms.stream.IMediaStream;
import com.wowza.wms.stream.MediaStreamActionNotifyBase;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ActionNotify extends MediaStreamActionNotifyBase {

    static WMSLogger logger = WMSLoggerFactory.getLogger(ActionNotify.class);
    static String CHANNEL_ACTIVE = "true";
    static String CHANNEL_INACTIVE = "false";
    static String TRANSCODED_SUFFIX = "_source";

    @Override
    public void onPublish(IMediaStream mediaStream, String streamName, boolean isRecord, boolean isAppend) {
        RecordingHelper helper = RecordingHelper.getInstance();
        if(mediaStream.isTranscodeResult() && streamName.endsWith(TRANSCODED_SUFFIX)) {
            String originalStreamName = streamName.substring(0, streamName.lastIndexOf(TRANSCODED_SUFFIX));
            helper.removeMediaStream(originalStreamName);
            helper.addMediaStream(streamName, mediaStream);
            return;
        }
        helper.addMediaStream(streamName, mediaStream);
        notifyPublishEvent(streamName, CHANNEL_ACTIVE);
    }

    @Override
    public void onUnPublish(IMediaStream mediaStream, String streamName, boolean isRecord, boolean isAppend) {
        if(mediaStream.isTranscodeResult()) {
            return;
        }
        String transcodedSourceStream = streamName + TRANSCODED_SUFFIX;
        RecordingHelper helper = RecordingHelper.getInstance();
        if(helper.removeMediaStream(streamName) == null) {
            helper.removeMediaStream(transcodedSourceStream);
        }
        LiveStreamRecorderMP4 recorder = helper.stopRecording(streamName);
        if(recorder == null) {
            // try with transcoded source stream
            recorder = helper.stopRecording(transcodedSourceStream);
        }
        if(recorder != null) {
            notifyPublishEvent(streamName, CHANNEL_INACTIVE, recorder);
            return;
        }
        notifyPublishEvent(streamName, CHANNEL_INACTIVE);
    }

    private void notifyPublishEvent(String streamName,  String action) {
        notifyPublishEvent(streamName, action, null);
    }

    private void notifyPublishEvent(String streamName, String action, LiveStreamRecorderMP4 recorder) {
        String serverURL = Server.getInstance().getProperties().getPropertyStr("server_url");

        StringBuilder parameters = new StringBuilder();
        parameters.append("channel=");
        parameters.append(streamName);
        parameters.append("&action=");
        parameters.append(action);
        if(recorder != null) {
            parameters.append("&file_name=");
            parameters.append(new File(recorder.getCurrentFile()).getName());
            parameters.append("&duration=");
            parameters.append( String.valueOf(recorder.getCurrentDuration()));
            parameters.append("&size=");
            parameters.append(String.valueOf(recorder.getCurrentSize()));
        }
        URL url = null;
        try {
            url = new URL(serverURL);
        } catch (MalformedURLException e) {
            logger.error("Error creating URL", e);
            return;
        }
        OutputStream outputStream = null;
        try {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");

            // For POST only - START
            connection.setDoOutput(true);
            outputStream = connection.getOutputStream();
            outputStream.write(parameters.toString().getBytes());
            outputStream.flush();

            if(connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                logger.error("Could not update channel, response code: " + connection.getResponseCode());
                return;
            }
        } catch (IOException e) {
            logger.error("Error sending request.", e);
        } finally {
            if(outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception ex){
                    // ignore.
                }
            }
        }
    }
}
