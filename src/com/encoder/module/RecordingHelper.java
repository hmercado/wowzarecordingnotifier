package com.encoder.module;

import com.wowza.wms.livestreamrecord.model.LiveStreamRecorderMP4;
import com.wowza.wms.stream.IMediaStream;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class RecordingHelper {

    private static RecordingHelper instance;
    private Map<String, LiveStreamRecorderMP4> recorders = new HashMap<>();
    private Map<String, IMediaStream> mediaStreamMap = new HashMap<>();


    public void startRecording(String channel, File targetFile, IMediaStream mediaStream) {
        LiveStreamRecorderMP4 recorder = new LiveStreamRecorderMP4();
        recorder.setRecordData(true);
        recorder.setStartOnKeyFrame(true);
        recorder.startRecording(mediaStream, targetFile.getPath(), targetFile.exists());
        recorders.put(channel, recorder);
    }

    public boolean isRecording(String channel) {
        return recorders.containsKey(channel);
    }

    public LiveStreamRecorderMP4 stopRecording(String channel) {
        if(!recorders.containsKey(channel)) {
            return null;
        }
        LiveStreamRecorderMP4 recorder = recorders.remove(channel);
        if(recorder == null) {
            return null;
        }
        recorder.stopRecording();
        return recorder;
    }

    public void addMediaStream(String streamName, IMediaStream mediaStream) {
        mediaStreamMap.put(streamName, mediaStream);
    }

    public IMediaStream getMediaStream(String streamName) {
        return mediaStreamMap.get(streamName);
    }

    public IMediaStream removeMediaStream(String streamName) {
        return mediaStreamMap.remove(streamName);
    }

    public static RecordingHelper getInstance() {
        if (instance == null) {
            instance = new RecordingHelper();
        }
        return instance;
    }

    public void release() {
        recorders.clear();
        mediaStreamMap.clear();
    }
}
