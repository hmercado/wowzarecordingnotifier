package com.encoder.http;

import com.encoder.module.RecordingHelper;
import com.wowza.wms.application.IApplication;
import com.wowza.wms.application.IApplicationInstance;
import com.wowza.wms.http.IHTTPRequest;
import com.wowza.wms.http.IHTTPResponse;
import com.wowza.wms.livestreamrecord.model.LiveStreamRecorderMP4;
import com.wowza.wms.stream.IMediaStream;
import com.wowza.wms.vhost.IVHost;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class HTTPLiveStreamRecorder extends GenericHTTPProvider {

    public static final String PATH = "recording";
    public static final String START_ACTION = "start";
    public static final String STOP_ACTION = "stop";
    static String TRANSCODED_SUFFIX = "_source";

    @Override
    public void onHTTPRequest(IVHost virtualHost, IHTTPRequest request, IHTTPResponse response) {
        String appName = request.getParameter("appname");
        String action = request.getParameter("action");
        String channel = request.getParameter("channel");
        String fileName = request.getParameter("filename");

        Map<String, String> fields = new HashMap<>();
        if (StringUtils.isEmpty(action)) {
            setErrorStatus(fields, "Action parameter is required.", PARAM_MISSED_CODE, false);
            sendJsonResponse(response, fields);
            return;
        }
        if (StringUtils.isEmpty(channel)) {
            setErrorStatus(fields, "Channel parameter is required.", PARAM_MISSED_CODE, false);
            sendJsonResponse(response, fields);
            return;
        }
        String transcodedChannel = channel + TRANSCODED_SUFFIX;
        RecordingHelper helper = RecordingHelper.getInstance();
        if(STOP_ACTION.equals(action)) {
            // stop original channel
            LiveStreamRecorderMP4 recorder = helper.stopRecording(channel);
            // if original channel has no recorder try with transcoded source channel.
            if(recorder == null) {
                recorder = helper.stopRecording(transcodedChannel);
            }
            fields.put("status", SUCESS_STATUS);
            if(recorder != null) {
                fields.put("file_name", new File(recorder.getCurrentFile()).getName());
                fields.put("duration", String.valueOf(recorder.getCurrentDuration()));
                fields.put("size", String.valueOf(recorder.getCurrentSize()));
            }
            sendJsonResponse(response, fields);
            return;
        }
        if(START_ACTION.equals(action) && (helper.isRecording(channel) || helper.isRecording(transcodedChannel))) {
            setErrorStatus(fields,  "El canal '" + channel + "' ya esta siendo grabado.", CHANNEL_ALREADY_RECORDING_CODE, true);
            sendJsonResponse(response, fields);
            return;
        }
        if (StringUtils.isEmpty(fileName)) {
            setErrorStatus(fields, "File name parameter is required.", PARAM_MISSED_CODE, false);
            sendJsonResponse(response, fields);
            return;
        }
        if (StringUtils.isEmpty(appName)) {
            setErrorStatus(fields, "Application name parameter is required.", PARAM_MISSED_CODE, false);
            sendJsonResponse(response, fields);
            return;
        }
        IApplication application = virtualHost.getApplication(appName);
        if (application == null) {
            logger.error("There is not application related to application name: " + appName);
            setErrorStatus(fields, "There is not application related to application name: " + appName, UNKNOWN_APP_CODE, false);
            sendJsonResponse(response, fields);
            return;
        }
        IApplicationInstance appInstance = application.getAppInstance(appName);
        if (appInstance == null) {
            logger.error("There is not application instance related to application name: " + appName);
            setErrorStatus(fields, "There is not application instance related to application name: " + appName, UNKNOWN_APP_CODE, false);
            sendJsonResponse(response, fields);
            return;
        }
        IMediaStream mediaStream = helper.getMediaStream(channel);
        if(mediaStream == null) {
            mediaStream = helper.getMediaStream(transcodedChannel);
        }
        if (mediaStream == null) {
            setErrorStatus(fields, "Existen problemas con la trasmision del canal, que impiden iniciar la grabacion.", CHANNEL_IS_NOT_ACTIVE_CODE, true);
            sendJsonResponse(response, fields);
            return;
        }
        File storageDir = new File(appInstance.getStreamStorageDir());
        File target = new File(storageDir, fileName);
        helper.startRecording(channel, target, mediaStream);
        fields.put("status", SUCESS_STATUS);
        sendJsonResponse(response, fields);
    }

    private void setErrorStatus(Map<String, String> fields, String message, String code, boolean showToUser) {
        fields.put("status", ERROR_STATUS);
        fields.put("code", code);
        fields.put("message", message);
        fields.put("show_error", String.valueOf(showToUser));
    }
}
