package com.encoder.http;

import com.coremedia.iso.IsoFile;
import com.googlecode.mp4parser.DataSource;
import com.googlecode.mp4parser.FileDataSourceImpl;
import com.wowza.wms.application.IApplication;
import com.wowza.wms.application.IApplicationInstance;
import com.wowza.wms.http.IHTTPRequest;
import com.wowza.wms.http.IHTTPResponse;
import com.wowza.wms.vhost.IVHost;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class HTTPVideoInfo extends GenericHTTPProvider {

    public static String PATH = "videoinfo";
    public static String FILE_NO_FOUND_CODE = "file-no-found";

    @Override
    public void onHTTPRequest(IVHost virtualHost, IHTTPRequest request, IHTTPResponse response) {
        String appName = request.getParameter("appname");
        String fileName = request.getParameter("filename");
        Map<String, String> fields = new HashMap<>();
        if (StringUtils.isEmpty(fileName)) {
            setErrorStatus(fields, "File name parameter is required.", PARAM_MISSED_CODE, false);
            sendJsonResponse(response, fields);
            return;
        }
        if (StringUtils.isEmpty(appName)) {
            setErrorStatus(fields, "Application name parameter is required.", PARAM_MISSED_CODE, false);
            sendJsonResponse(response, fields);
            return;
        }
        IApplication application = virtualHost.getApplication(appName);
        if (application == null) {
            logger.error("There is not application related to application name: " + appName);
            setErrorStatus(fields, "There is not application related to application name: " + appName, UNKNOWN_APP_CODE, false);
            sendJsonResponse(response, fields);
            return;
        }
        IApplicationInstance appInstance = application.getAppInstance(appName);
        if (appInstance == null) {
            logger.error("There is not application instance related to application name: " + appName);
            setErrorStatus(fields, "There is not application instance related to application name: " + appName, UNKNOWN_APP_CODE, false);
            sendJsonResponse(response, fields);
            return;
        }
        File storageDir = new File(appInstance.getStreamStorageDir());
        File video = new File(storageDir, fileName);
        if(!video.exists()) {
            setErrorStatus(fields,String.format("El archivo %s fue borrado o movido.", fileName) , FILE_NO_FOUND_CODE, true);
            sendJsonResponse(response, fields);
            return;
        }
        long duration = getDuration(video);
        fields.put("duration", String.valueOf(duration));
        fields.put("size", String.valueOf(video.length()));
        fields.put("status", SUCESS_STATUS);
        sendJsonResponse(response, fields);
    }

    private void setErrorStatus(Map<String, String> fields, String message, String code, boolean showToUser) {
        fields.put("status", ERROR_STATUS);
        fields.put("code", code);
        fields.put("message", message);
        fields.put("show_error", String.valueOf(showToUser));
    }

    public synchronized long getDuration(File recordedVideoFile) {
        long lengthInMillis = 0;
        IsoFile isoFile = null;
        try {
            DataSource fdata = new FileDataSourceImpl(recordedVideoFile);
            isoFile = new IsoFile(fdata);
            double lengthInSeconds = (double) isoFile.getMovieBox().getMovieHeaderBox().getDuration()
                    / isoFile.getMovieBox().getMovieHeaderBox().getTimescale();
            lengthInMillis = Double.valueOf(lengthInSeconds * 1000).longValue();
        } catch (IOException e) {
            logger.error("Error getting duration from file: " + recordedVideoFile.getName(), e);
        } finally {
            if (isoFile != null) {
                try {
                    isoFile.close();
                } catch (IOException e) {
                    logger.error("Error closing iso file: " + recordedVideoFile.getName(), e);
                }
            }
        }
        return lengthInMillis;
    }
}
