package com.encoder.http;

import com.wowza.util.Base64;
import com.wowza.wms.http.IHTTPRequest;
import com.wowza.wms.http.IHTTPResponse;
import com.wowza.wms.vhost.IVHost;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Created by Hugo on 5/07/2016.
 */
public class HTTPAuthInfo extends GenericHTTPProvider {

    public static final String AUTH_PASSWORD_FILE_PATH = "%s/conf/publish.password";

    public void onHTTPRequest(IVHost virtualHost, IHTTPRequest request, IHTTPResponse response) {
        Map<String, String> fields = new HashMap<>();
        String pwdFilePath = String.format(AUTH_PASSWORD_FILE_PATH, virtualHost.getHomePath());
        File pwdFile = new File(pwdFilePath);
        if (!pwdFile.exists()) {
            fields.put("error", "No se encontro el archivo de autenticacion.");
            sendJsonResponse(response, fields);
            return;
        }
        FileInputStream fstream = null;
        try {
            fstream = new FileInputStream(pwdFile);
        } catch (FileNotFoundException e) {
            fields.put("error", "No se pudo leer el archivo de autenticacion.");
            sendJsonResponse(response, fields);
            return;
        }
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fstream));
        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                if(line == null || line.trim().isEmpty()) {
                    continue;
                }
                line = line.trim();
                if(line.startsWith("#")) {
                    continue;
                }
                StringTokenizer tokenizer = new StringTokenizer(line, " ");
                String user = tokenizer.nextElement().toString();
                String pwd = tokenizer.nextElement().toString();

                String auth = String.format("%s:%s", user, pwd);
                auth = Base64.encodeBytes(auth.getBytes());

                fields.put("success", "");
                fields.put("auth", auth);
                sendJsonResponse(response, fields);
                return;
            }
        } catch (IOException e) {
            fields.put("error", "Hubo un error al leer las lineas del archivo de autenticacion.");
        } finally {
            try {
                if(bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
            }
        }
        fields.put("error", "El proxeso no genero ningun resultado.");
        sendJsonResponse(response, fields);
    }
}
