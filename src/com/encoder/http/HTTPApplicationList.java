package com.encoder.http;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.wowza.wms.http.IHTTPRequest;
import com.wowza.wms.http.IHTTPResponse;
import com.wowza.wms.logging.WMSLogger;
import com.wowza.wms.logging.WMSLoggerFactory;
import com.wowza.wms.vhost.IVHost;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by Hugo on 27/06/2016.
 */
public class HTTPApplicationList extends GenericHTTPProvider {

    static WMSLogger logger = WMSLoggerFactory.getLogger(HTTPApplicationList.class);

    @Override
    public void onHTTPRequest(IVHost virtualHost, IHTTPRequest request, IHTTPResponse response) {
        response.setHeader("Content-Type", "application/json");
        JsonFactory jsonFactory = new JsonFactory();
        JsonGenerator generator = null;
        try {
            List<String> applications = new ArrayList<>(virtualHost.getApplicationFolderNames()) ;
            Collections.sort(applications);
            generator = jsonFactory.createGenerator(response.getOutputStream());
            generator.writeStartObject();
            generator.writeFieldName("applications");
            generator.writeStartArray();
            for (String application : applications) {
                generator.writeString(application);
            }
            generator.writeStartArray();
            generator.writeEndObject();
        } catch (IOException ex) {
            logger.error("No se pudo generar el listado de aplicaciones.", ex);
        } finally {
            if(generator != null) {
                try {
                    generator.close();
                } catch (IOException e) {
                }
            }
        }
    }
}
