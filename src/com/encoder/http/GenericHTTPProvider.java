package com.encoder.http;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.wowza.wms.http.HTTProvider2Base;
import com.wowza.wms.http.IHTTPRequest;
import com.wowza.wms.http.IHTTPResponse;
import com.wowza.wms.logging.WMSLogger;
import com.wowza.wms.logging.WMSLoggerFactory;
import com.wowza.wms.vhost.IVHost;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.Map;

public class GenericHTTPProvider extends HTTProvider2Base {

    static WMSLogger logger = WMSLoggerFactory.getLogger(GenericHTTPProvider.class);
    public static String ERROR_STATUS = "error";
    public static String SUCESS_STATUS = "success";
    public static String PARAM_MISSED_CODE = "param-missed";
    public static String CHANNEL_ALREADY_RECORDING_CODE = "channel-recording";
    public static String CHANNEL_IS_NOT_ACTIVE_CODE = "channel-no-active";
    public static String UNKNOWN_APP_CODE = "unknown-app";
    private HTTPLiveStreamRecorder httpLiveStreamRecorder = null;
    private HTTPVideoInfo httpVideoInfo = null;

    public void onHTTPRequest(IVHost virtualHost, IHTTPRequest request, IHTTPResponse response) {
        if(HTTPLiveStreamRecorder.PATH.equals(request.getRequestURL())) {
            if(httpLiveStreamRecorder == null) {
                httpLiveStreamRecorder = new HTTPLiveStreamRecorder();
            }
            httpLiveStreamRecorder.onHTTPRequest(virtualHost, request, response);
            return;
        } else if (HTTPVideoInfo.PATH.equals(request.getRequestURL())) {
            if(httpVideoInfo == null) {
                httpVideoInfo = new HTTPVideoInfo();
            }
            httpVideoInfo.onHTTPRequest(virtualHost, request, response);
        }
    }

    protected void sendJsonResponse(IHTTPResponse response, Map<String, String> fields) {
        response.setHeader("Content-Type", "application/json");
        JsonFactory jsonFactory = new JsonFactory();
        JsonGenerator generator = null;
        try {
            generator = jsonFactory.createGenerator(response.getOutputStream());
            generator.writeStartObject();
            for (Map.Entry<String, String> e : fields.entrySet()) {
                generator.writeStringField(e.getKey(), e.getValue());
            }
            generator.writeEndObject();
        } catch (IOException e) {
            logger.fatal("Could not write response.", e);
        } finally {
            if (response.getOutputStream() != null) {
                try {
                    response.getOutputStream().close();
                } catch (Exception e) {
                    logger.fatal("Probably closing output stream", e);
                }
            }
            if (generator != null) {
                try {
                    generator.close();
                } catch (IOException e) {
                    logger.fatal("Probably closing json generator", e);
                }
            }
        }
    }
}
